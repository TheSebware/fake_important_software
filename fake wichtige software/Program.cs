﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Reflection;

/*
 * Copyright (c) 2013 TheSebware [mailto:TheSebware@gmail.com]
 * Diese Software wird so wie sie ist übergeben,
 * BENUTZUNG AUF EIGENE GEFAHR!
 * 
 * Die folgenden Dateien stammen nicht von mir - Zu Quell-/Urheberrechtsangaben ziehen Sie (sofern nicht anders angegeben) die jeweilige Datei zu Rat!
 *  - CfgDatei.cs - ENTSPRICHT NICHT NACHFOLGENDEM LIZENZTEXT
 * 
 * Diese Software ist unter der TheSebware-OSS-1.0_C-Lizenz veröffentlicht:
 *  Der einfachheithalber ist "der Autor" mit "die Autorin" oder "die Autoren" gleichzusetzen!
 *  Sie dürfen:
 *      - diese Software kostenlos verwenden, dabei darf Werbung geschaltet werden
 *      - diese Software OHNE NENNENSWERTEN* GEWINN verbreiten
 *      - die Software bearbeiten und weitergeben, solange
 *          a) die Software im Quelltext und in Binärer Form KOSTENLOS erhältlich ist,
 *             Werbung darf geschalten werden.
 *             Des Weiteren gilt auch hier: KEIN NENNENSWERTER* GEWINN bei der Verbreitung wird als kostenlos gewertet.
 *          b) keine Copyrighthinweise verändert/entfernt wurden.
 * 
 *  Sie dürfen NACH EINWILLIGUNG DES AUTORS/DER AUTOREN
 *      - diese Software auf beigelegte CDs/DVDs/Blu-Ray-Disks/andere Speichermedien irgendwelcher Fachliteratur Verbreiten
 *      - die Software wie folgt Kommerziell vertreiben: 40 % des Reinerlöses sind an den Vorgängerautor/die Vorgängerautoren zu zahlen.
 *        Sie können auch andere Beteiligungen mit dem letzten Urheber vereinbaren. In diesem Fall gelten die 40 % Beteiligung nicht.
 *        Der Vorautor ist in jedem Fall verpflichtet, mit dem jeweiligen Vor-Vorautor sich die Zustimmung dafür einzuholen. Dies ist solange zu wiederholen,
 *        bis der letzte Autor zustimmt.
 * 
 *  Sie dürfen NICHT:
 *      - eine oder mehrere dieser Modifikationen durchführen
 *          - Copyrighthinweise des jeweiligen Rechteinhabers entfernen oder modifizieren
 *          - Werbung zu entfernen, modifizieren oder deren Anzeige anderweitig nicht so angebracht wird,
 *            wie es der Autor geplant hat
 *         
 *  Sollten Sie den Autor jemals treffen und die Software für wertvoll genug erachten, geben Sie ihm bitte eine Coca Cola (notfalls auch Pepsi) aus.
 *            
 *  Anhänge:
 *      * Nicht-Nennenswerter Gewinn: Ertrag, welcher entsteht, um für "rundere Preise" (von 1,88 € -> 2 € Gewinn 0,12 €) zu erhalten
 *      ** Verkauf wird jeglicher Gewinn bezeichnet, der nicht mehr nennenswert ist.
 *  
 *  ACHTUNG: Sie können mit dem Autor Kontakt aufnehmen und jede Regelung ändern!
 * 
*/
namespace org.bitbucket.TheSebware.fake_wichtige_software {
    class Program {
        //static CfgFile conf = new CfgFile();
        static void Main(string[] args) {
            //string cfgpath = String.Concat(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "\\fws.ini");


            randomgenerator r = new randomgenerator(101);
            while (true) {
                Console.Write(r.alphanumericWithLF());
            }
        }
             
    }
    //Wenn mir einer erklärt, wie ich folgenden Teil in eine eigene Datei auslagere...
    public class randomgenerator {
        private Random r;
        private Boolean initialized = false;
        public randomgenerator(int seed) {
            r = new Random(seed);
            initialized = true;
        }
        public char everyASCII() {
            if (!initialized) { return tochar(0); }
            updateClass();
            return tochar(r.Next(0x00, 0x80));
        }

        public char everyPrintable() {
            if (!initialized) { return tochar(0); }
            updateClass();
            int i = r.Next(0x20, 0x7F);
            if (i == 0x20) { return '\n'; }
            else { return tochar(i); }
        }

        public char alphanumeric() {
            if (!initialized) { return tochar(0); }
            updateClass();
            int i = r.Next(0x30, 0x6E);
            if (i > 0x39) { i = i + 8; }
            if (i > 0x5A) { i = i + 7; }
            return tochar(i);
        }

        public char alphanumericWithLF() {
            if (!initialized) { return tochar(0); }
            updateClass();
            if (r.Next(1, 200) == 1) { return '\n'; }
            updateClass();
            int i = r.Next(0x30, 0x6E);
            if (i > 0x39) { i = i + 8; }
            if (i > 0x5A) { i = i + 7; }
            return tochar(i);
        }

        //Tippfaulheit:
        private char tochar(int i) {
            return Convert.ToChar(i);
        }

        //mir ist kein besserer Name eingefallen...
        private void updateClass() {
            int i = r.Next();
            r = null;
            r = new Random(i);
        }
    }
}